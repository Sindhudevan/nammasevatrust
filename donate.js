
var tempOptions = ["Vishnu Temple", "Sholinga Swamy", "Bhoomi Neela"];
var StateOptions = ["TamilNadu", "Karnataka", "Kerala"];
var loadKey = "";
let poojaFields = [];
var donateData = [];

let poojaInputDiv = ``

const donateForm = {
    templeId: "",
    name: "",
    mobileNumber: "",
    email: "",
    address: "",
    pincode: "",
    state: "",
    country: "",
}

const poojaInfo = {
    occationId: "",
    poojaNotes: "",
    personName: "",
    gothra: "",
    nakshatraId: "",
    occasionDate: "",
}

var radioBtnValue = 0;

var poojaCount = ['1-Pooja', '2-Pooja', '3-Pooja', '4-Pooja'];

const poojaCountObj = [
    {
        name: '1-Pooja',
        checked: true,
    },
    {
        name: '2-Pooja',
        checked: false,
    },
    {
        name: '3-Pooja',
        checked: false,
    },
    {
        name: '4-Pooja',
        checked: false,
    }
]



function onRender() {
    templeOptions();
    stateOptions();
    capcha();
    poojaOption();
    radioChange(0);
}



function templeOptions(key) {
    if (key === 1) {
        for (let [index, item] of tempOptions.entries()) {
            var option = document.createElement("option");
            let select = document.getElementById("selectField");
            option.text = item;
            option.value = index;
            select.appendChild(option);
        }
    }
}

function stateOptions(key) {
    if (key === 1) {
        for (let [index, item] of StateOptions.entries()) {
            var option = document.createElement("option");
            let select = document.getElementById("stateField");
            option.text = item;
            option.value = index;
            select.appendChild(option);
        }
    }
}





function capcha(key) {
    let firstNumber = Math.floor(Math.random() * 50);
    let lastNumber = Math.floor(Math.random() * 50);
    document.getElementById('capchaNumbr').innerHTML = firstNumber + " + " + lastNumber;
}

function handleChange(name, value) {
    donateForm[name] = document.getElementById(name).value;
    console.log(donateForm)
}


function donateOnSubmit() {
    let isValidate = true;
    let mailValidate = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    Object.keys(donateForm).map((item, index) => {
        switch (item) {
            case "email":
                let emailValid = mailValidate.test(donateForm["email"]);
                if (donateForm["email"] === "") {
                    document.getElementsByClassName("errorMsg")[index].innerHTML = "Please Fill this Content";
                    document.getElementsByClassName("errorMsg")[index].style.visibility = "visible";
                } else if (!emailValid) {
                    document.getElementsByClassName("errorMsg")[index].innerHTML = "Entered Email Id is Incorrect";
                    document.getElementsByClassName("errorMsg")[index].style.visibility = "visible";
                }
                break;

            default:
                break;
        }
    })
}







function poojaOption() {
    let radioDiv = document.getElementById('poojaOption');
    radioDiv.innerHTML = "";
    for (let i = 0; i < poojaCountObj.length; i++) {
        radioDiv.innerHTML += `<div class="layer" style="border-color:${poojaCountObj[i].checked ? "red" : ""}"><div class="innerLayer" style="background-color:${poojaCountObj[i].checked ? "red" : ""}">
      <input class="radioBtn" 
      onClick="radioChange(${i})" ${poojaCountObj[i].checked && "checked"}      
     type="radio" 
      /></div></div> ${poojaCountObj[i].name}`;
    }
}


function radioChange(id) {
    let inputDiv = document.getElementById('poojaDetails');
    inputDiv.innerHTML = "";
    poojaFields = [];
    let count = id + 1;
    for (let i = 0; i < poojaCountObj.length; i++) {
        if (i === id) {
            poojaCountObj[i].checked = true;
        } else {
            poojaCountObj[i].checked = false;
        }
    }
    poojaOption();
    for (j = 0; j < count; j++) {
        poojaFields.push(poojaInfo);
    }
    for (k = 0; k < poojaFields.length; k++) {
        inputDiv.innerHTML += `<div>
        <div class="poojaDetailTitle">
            <p>Enter Your ${k + 1} Pooja Details</p>
            <hr>
        </div>
        <div style="margin-bottom: 80px;">
            <div class="textFields">
                <div>
                    <select id="occationId" name="country" value="" name="occationId" onchange="poojaOnChange(${k},name,event.target.value)" placeholder="Enter Your Country" id="stateField"
                        class="inputField">
                        <option>Select the Occasion</option>
                    </select>
                    <span class="errorMsg"></span>
                </div>
                <div>
                    <input id="poojaNotes" name="poojaNotes" value="" onchange="poojaOnChange(${k},name,event.target.value)" placeholder="Pooja Notes (Name of Occasion)"
                        class="inputField"></input>
                    <span class="errorMsg"></span>
                </div>
                <div>
                    <input id="poojaFor" name="poojaFor" value="" onchange="poojaOnChange(${k},name,event.target.value)" placeholder="Pooja for(Name of Person)" class="inputField"></input>
                    <span class="errorMsg"></span>
                </div>
            </div>
            <div class="textFields">
                <div>
                    <input id="gothra" name="gothra" value="" onchange="poojaOnChange(${k},name,event.target.value)" placeholder="Gothra" class="inputField"></input>
                    <span class="errorMsg"></span>
                </div>
                <div>
                    <select id="nakshatraId" name="nakshatraId" value="" onchange="poojaOnChange(${k},name,event.target.value)" placeholder="" id="stateField" class="inputField">
                        <option>Select Nakshatra</option>
                    </select>
                    <span class="errorMsg"></span>
                </div>
                <div>
                    <input id="occasionDate" type="date" onchange="poojaOnChange(${k},name,event.target.value)" name="occasionDate" value="" placeholder="Occasion Date (dd/mm/yyyy)" class="inputField"></input>
                </div>
            </div>
            <div class="dateNoteBlock">
                <div class="dateNote">
                    If the occasion needs to be performed according to the Tamil calendar, please give
                    the corresponding English dates
                </div>
            </div>
        </div>
        </div>
        </div>`;
    }

}


function poojaOnChange(index, name, value) {
    poojaInfo[name] = document.getElementById(name).value;
    poojaFields[index][name] = value;
}












