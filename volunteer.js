const volunteerForm = {
    name: "",
    age: "",
    qualification: "",
    email: "",
    contact: "",
    address: "",
}


function onRender() {
    capcha();
}

function capcha() {
    let firstNumber = Math.floor(Math.random() * 50);
    let lastNumber = Math.floor(Math.random() * 50);
    document.getElementById('volCapcha').innerHTML = firstNumber + " + " + lastNumber;

}

function volunteerOnChange(name, value) {
    volunteerForm[name] = document.getElementById(name).value;
}


function volunteerOnSubmit() {
        let isValidate = true;
        let mailValidate = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        Object.keys(volunteerForm).map((item, index) => {
            console.log(item)
            if (volunteerForm[item] === "") {
                document.getElementsByClassName("errorMsg")[index].innerHTML = "Please Fill the Field";
                document.getElementsByClassName("errorMsg")[index].style.visibility = "visible";
                isValidate = false;
            } else if (item === "email") {
                let emailValid = mailValidate.test(volunteerForm["email"]);
                if (emailValid) {
                    isValidate = true;
                } else {
                    document.getElementsByClassName("errorMsg")[index].innerHTML = "Email id is Incorrect";
                    document.getElementsByClassName("errorMsg")[index].style.visibility = "visible";
                    isValidate = false;
                }
            } else if (item === "contact") {
                console.log(volunteerForm["contact"])
                if (volunteerForm[item] < 10) {
                    document.getElementsByClassName("errorMsg")[index].innerHTML = "Mobile Number is Incorrect";
                    document.getElementsByClassName("errorMsg")[index].style.visibility = "visible";
                    isValidate = false;

                }
            }
        })
    
}
