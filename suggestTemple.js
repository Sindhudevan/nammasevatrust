const tempForm = {
    tempName: "",
    deity: "",
    tempAddress: "",
    vaidekaName: "",
    vaidekaContact: "",
    vaidekaIncome: "",
    personName: "",
    personEmail: "",
    personContact: "",

}



function onRender() {
    capcha();
}

function capcha() {
    let firstNumber = Math.floor(Math.random() * 50);
    let lastNumber = Math.floor(Math.random() * 50);
    document.getElementById('tempCapcha').innerHTML = firstNumber + " + " + lastNumber;

}


function SuggTempOnChange(name, value) {
    tempForm[name] = document.getElementById(name).value;
    console.log(tempForm)
}


function TempOnSubmit(i) {
    console.log(i)
    let isValidate = true;
    let mailValidate = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    Object.keys(tempForm).map((item, index) => {
        switch (item) {
            case "personEmail":
                let emailValid = mailValidate.test(tempForm["personEmail"]);
                if (tempForm["personEmail"] === "") {
                    document.getElementsByClassName("errorMsg")[index].innerHTML = "Please Fill this Content";
                    document.getElementsByClassName("errorMsg")[index].style.visibility = "visible";
                } else if (!emailValid) {
                    document.getElementsByClassName("errorMsg")[index].innerHTML = "Entered Email Id is Incorrect";
                    document.getElementsByClassName("errorMsg")[index].style.visibility = "visible";
                }
                break;

            default:
                break;

            case "personContact":
                if (item === "personContact") {
                    
                    if (tempForm["personContact"] === "") {
                        console.log('empty')
                        document.getElementsByClassName("errorMsg")[index].innerHTML = "Please Fill the Field";
                        document.getElementsByClassName("errorMsg")[index].style.visibility = "visible";
                        isValidate = false;
                    } else if (tempForm["personContact"].length < 10) {
                        console.log('not a mobile')
                        console.log(tempForm["personContact"])
                        document.getElementsByClassName("errorMsg")[index].innerHTML = "Mobile Number is Incorrect";
                        document.getElementsByClassName("errorMsg")[index].style.visibility = "visible";
                        isValidate = false;
                    } else{
                        document.getElementsByClassName("errorMsg")[index].innerHTML = "";
                        document.getElementsByClassName("errorMsg")[index].style.visibility = "hidden";
                        isValidate = true;

                    }
                }
        }
    })

}
